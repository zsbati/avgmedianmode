n = int(input().strip())
string = input().strip()
arr = string.split(" ")
arr1 = []
arr1 = [int(st) for st in arr]
arr1.sort()

sum = 0;      
for i in range(n):
  sum += arr1[i]
    
avg = sum/n
print("avg= "+str(avg));

median = 0
index = 0
if n%2 == 0:
  index = int(n/2) - 1 #counting from 0
  median = (arr1[index] + arr1[index+1])/2
else:
  index = int(n/2)
  median = arr1[index]
  
print("median= "+str(median))

frequencies = dict()

for num in arr1:
  frequencies[num] = frequencies.get(num, 0) + 1
print(frequencies)  

max_freq = frequencies.get(arr1[0])
for num in arr1:
  if max_freq < frequencies.get(num):
    max_freq = frequencies.get(num)

max_list = []
for num in arr1:
  if max_freq == frequencies.get(num):
    max_list.append(num)
max_list.sort()
mode = max_list[0]
print("the mode is: "+str(mode))    


