import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;


public class Main {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        /*Scanner sc=new Scanner(System.in);
        int n = sc.nextInt();
        String s = sc.next();
        sc.close();*/
        int n = 11;
        String s = "11 2 1 2 3 6 6 5 5 5 4";
        String[] numbers = s.split(" ");
        //System.out.println(numbers[0]);
        int[] nums = new int[n];
        for (int i=0; i<n; i++){
          nums[i] = Integer.valueOf(numbers[i]);
        }
        
        Arrays.sort(nums);

        int sum = 0;      
        for (int i=0; i<n; i++){
          sum += nums[i];
        }

        float avg = ( (float) sum)/( (float) n);
        System.out.println("avg= "+avg);

        float median = 0;
        if (n%2==0){
          median = ( (float) (nums[n/2] + nums[n/2+1]))/((float) 2);
        } else {
          median = nums[n/2+1];
        }

        System.out.println("median= " +median);

        HashMap < Integer, Integer > numbers_map = new HashMap <Integer, Integer> ();
        for(Integer number : nums){

            if(!numbers_map.containsKey(number)){

              numbers_map.put(number, 1);

            } else {
              numbers_map.put(number, (numbers_map.get(number)+1));
            }

        }

        
        int maxFreq = 0;
        for(int number : nums){
          if (numbers_map.get(number) > maxFreq){
            maxFreq = numbers_map.get(number);
          }
        }

        
        List<Integer> modes = new LinkedList<Integer>(); 
        for(int i : nums){
            if(!modes.contains(i) && numbers_map.get(i) == maxFreq){
                modes.add(i);
            }    
        }

        Collections.sort(modes);
        System.out.println("mode= " +modes.get(0));

    } //method over
} //class ends
